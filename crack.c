#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings
int LENGTH=0;
// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess
    // Compare the two hashes
    //printf("%s\n", hash);
    //printf("%s\n", guess);
    
    //printf("%s\n", guesshash);
    return strncmp(hash, guess, 32);
    
    free(hash);
    free(guess);
    // Free any malloc'd memory
}

// TODO
// Read in the hash file and return the array of strings.
char **read_hashes(char *filename)
{
    // Malloc space for entire file
    // Get size of the file
    struct stat st;
    if (stat(filename, &st) == -1)
    {
        fprintf(stderr, "Can't get info about %s\n", filename);
        exit(1);
    }
    int len = st.st_size;
    char *file = malloc(len);

    //Read entire file into memory
    FILE *f = fopen(filename, "r");
    if(!f){
        printf("cant open %s for read\n", filename);
        exit(1);
    }
    fread(file, 1, len, f);
    fclose(f);
    
    
    // Replace \n with \0
    int count = 0;
    for(int i = 0; i < len; i++){
        if(file[i]=='\n'){
            file[i]='\0';
            count++;
        }
    
    }
    
    // Malloc space for array of pointers
    char **line = malloc((count+1)*sizeof(char*)*33);

    //Fill in addresses
    int word = 0;
    line[word] = file;  //first word in the file
    word++;
    for(int i = 1; i < len; i++){
        if(file[i] == '\0' && i+1<len){
            line[word]=&file[i+1];
            word++;
        }
    }
    
    LENGTH = word;
    line[word]=NULL;

    //Return address of second array

    return line;
    
    free(file);
}


// TODO
// Read in the dictionary file and return the data structure.
// Each entry should contain both the hash and the dictionary
// word.
char **read_dict(char *filename)
{
    FILE *f;
    f = fopen(filename, "r");
    
    if(f==NULL){
        printf("Couldn't open file for reading\n");
        exit(1);
    }
    
    char password[30];
    while(fscanf(f, "%s", password) != EOF){
        password[strlen(password)] = '\0';
        char *hash = md5(password, strlen(password));
        
        FILE *d;
        d = fopen("dictionary.txt", "a");
        if(d==NULL){
            printf("Couldn't open file for reading\n");
            exit(1);
        }
        
        fprintf(d, "%s %s\n", hash, password);
        fclose(d);

        free(hash);
    }
    fclose(f);
    
    char ** dictionary = read_hashes("dictionary.txt");
    
    return dictionary;
}

int compare(const void *a, const void *b){
    return tryguess((char *)a, *(char **)b);
}

int sort(const void *a, const void *b){
    return strcmp(*(char **)a, *(char **)b);                  //ordered alphabatized
    
    /*int diff = strlen(*(char**)a) - strlen(*(char**)b);         //ordered by length
    if(diff ==0)
        return strcmp(*(char **)b, *(char **)a);
    else
        return diff;*/
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);
    int length_hashes = LENGTH;

    // TODO: Read the dictionary file into an array of strings
    char **dict = read_dict(argv[2]);
    char * save = dict[0];
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, LENGTH, sizeof(char *), sort);
    /*int i = 0;
    while(i<100){
        printf("%s\n", dict[i]);
        i++;
    }*/

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    int i = 0;
    while(i<length_hashes){
        char ** found = bsearch(hashes[i], dict, LENGTH , sizeof(char *), compare);
        if(found){
            printf("Found Password: %s\n", *found);
        }
        i++;
    }
    
    free(hashes[0]);
    free(save);
    free(hashes);
    free(dict);
}
